/****************************************
* NombrePrograma:
* Descripcion:
*
* Autor: Heber Quequejana
* Fecha:
*
****************************************/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <string.h>
#define MAX 11000

using namespace std;

///ESTRUCTURAS

struct sFECHA
{
  unsigned int dia;
  unsigned int mes;
  unsigned int anio;
};

struct stRegistro
{
   char valido; ///indica la validez del registro
   unsigned int numero;
   unsigned int codigo;
   char nombre[81];
   float deuda;
   sFECHA fecha;
};

struct stIndice{
   unsigned int codigo;
   long indice;
};

///PROTOTIPOS DE FUNCION

///Funciones auxiliares
void escribeCad(const char *texto, int fila, int columna);
void centraCad(const char *texto, int fila);
char *repiteCad(int longitud, char caracter);
unsigned char *IngresaCad(int fila, int columna, int longitud);
int ingresaEnt(int fila, int columna, int longitud);
void salir(char *file,int error);
void salir(char *file);

///Funciones principales
int menu(char *file);
void leerCSV(char *file);
void seleccionarFile(char *file);
void (*opciones[])(char *file) = {
   salir,
   seleccionarFile,
   leerCSV
};

void seleccionarFile(char *file)
{
   const char archivo[3][51]={"archivo00.csv","archivo01.csv","archivo02.csv"};
   int opcion;
   system("cls");
   centraCad("ELIJA EL ARCHIVO CSV CON EL QUE DESEA TRABAJAR :",2);
   escribeCad("0. CANCELAR",5,20);
   escribeCad("1. ARCHIVO 00",6,20);
   escribeCad("2. ARCHIVO 01",7,20);
   escribeCad("3. ARCHIVO 02",8,20);
   escribeCad("INGRESE SU OPCION ---> ",10,15);
   do{
      opcion = ingresaEnt(10,38,3);
   }while(opcion>3 || opcion<0);
   if(opcion == 0){
      return;
   }
   else{
      strcpy(file,archivo[opcion-1]);
   }
}

///Principal
int main()
{
   char nombre[15]={"archivo00.csv"};  ///cargando el archivo por defecto
   for(;;){
      (*opciones[menu(nombre)])(nombre);
   }
   system("pause");
   return 0;
}

int menu(char *file)
{
   int opcion=-1;
   system("cls");
   centraCad("MENU PRINCIPAL - ARCHIVOS",2);
   centraCad("=========================",3);
   gotoxy(20,5);
   printf("SE ESTA USANDO EL FILE: \"%s\"",file);
   textcolor(LIGHTRED);
   escribeCad("0. SALIR",7,20);
   textcolor(LIGHTGRAY);
   escribeCad("1. ELEGIR ARCHIVO A USAR",8,20);
   escribeCad("2. MOSTRAR CONTENIDO DEL ARCHIVO",9,20);
   escribeCad("3. ################",10,20);
   escribeCad("4. ################",11,20);
   escribeCad("5. ################",12,20);
   escribeCad("6. ################",13,20);
   escribeCad("INGRESE SU OPCION ---> ",15,15);
   do{
      opcion = ingresaEnt(15,38,3);
   }while(opcion>6 || opcion<0);
   return opcion;
}

void centraCad(const char *texto, int fila)
{
   int columna;
   columna=(80-strlen(texto))/2;
   escribeCad(texto,fila,columna);
}

void escribeCad(const char *texto, int fila, int columna)
{
   gotoxy(columna,fila);
   clreol();
   printf("%s",texto);
}

char *repiteCad(int longitud, char caracter)
{
   char *cadena;
   cadena = (char*)malloc((longitud+1)*sizeof(char));
   memset(cadena,0,longitud+1);/// inicia el espacio en la cadena
   memset(cadena,caracter,longitud);
   return cadena;
}

char *ingresaCad(int fila, int columna, int longitud)
{
   const char asterisco = '*';
   const char nulo = '\0';
   const char enter = 13;
   int posicion;
   char caracter;
   char *cadena;
   cadena = (char*)malloc(longitud+1);

   //iniciar contenido de la cadena con asteriscos
   strcpy(cadena,repiteCad(longitud,asterisco));

   //incializar posici�n del caracter en la cadena
   posicion = 0;
   do{
      escribeCad(cadena,fila,columna);
      gotoxy(columna+posicion,fila);
      clreol();
      caracter = getch();
      if (caracter>=32)//caracter imprimible
      {
         cadena[posicion] = caracter;
         posicion = posicion + 1;
      }
      if (caracter == 8)  //backspace
      {
         if (posicion>0)
         {
            cadena[posicion] = 0;
            posicion = posicion - 1;
            cadena[posicion] = asterisco;
         }
      }
   }while(caracter!=enter && posicion!=longitud);

   if (posicion == longitud){
      escribeCad(cadena,fila,columna);
   }
   //borrar los asteriscos de la cadena
   char *posast = strchr(cadena,asterisco);
   if (posast) //existe un asterisco en la cadena ingresada
   {
      *posast = nulo;   //corta la cadena
   }

   return cadena;
}

int ingresaEnt(int fila, int columna, int longitud)
{
   int numero;
   char *cadena;
   cadena = (char*)malloc(longitud+1);
   do{
      strcpy(cadena,ingresaCad(fila,columna,longitud));
   }while(cadena[0]<47||cadena[0]>57);
   numero = atoi(cadena);
   return numero;
}

void salir(char *file, int error)
{
   system("cls");
   textcolor(LIGHTRED);
   if (error==0){
      centraCad("SALIENDO DEL PROGRAMA...",5);
   }
   else{
      centraCad("\aERROR AL ABRIR EL ARCHIVO :",5);
      gotoxy(32,7); printf("\"%s\"",file);
      centraCad("SALIENDO DEL PROGRAMA...",9);
   }
   textcolor(LIGHTGRAY);
   printf("\n\n");
   exit(error);
}

void salir(char *file)
{
   salir(file,0);
}

void leerCSV(char *file)
{
   FILE *F;
   stRegistro A;
   char V[100],*ptr;
   char cab[10][20];
   char mes[11];
   int i;
   F = fopen(file,"r");
   if(F == NULL){
      system("cls");
      salir(file,1);
   }
   centraCad("CONTENIDO DEL ARCHIVO CSV",2);
   centraCad("=========================",3);
   //printf("\n\t\tCONTENIDO DEL ARCHIVO: \n\n");

   ///LEYENDO LAS CABECERAS
   printf("\n\n\r===================================================================\n");
   fgets(V,100,F);
   for (i=0; i<5; ){
      if (i == 0){
         ptr = strtok(V,",");
      }
      else{
         ptr = strtok(NULL,",");
      }
      strcpy((cab)[i],ptr);
      printf("%s\t",(cab)[i]);
      i = i + 1;
   }
   printf("\r-------------------------------------------------------------------\n");

   ///LEYENDO EL CONTENIDO
   fgets(V,MAX,F);
   while(!feof(F))
   {
      ///numero
      ptr = strtok(V,",");
      A.numero = atoi(ptr);
      ///codigo
      ptr = strtok(NULL,",");
      A.codigo = atoi(ptr);
      ///nombre
      ptr = strtok(NULL,",");
      strcpy(A.nombre,ptr);
      ///deuda
      ptr = strtok(NULL,",");
      A.deuda = atof(ptr);
      ///Fecha de vencimiento
      ptr = strtok(NULL,"/");
      A.fecha.dia = atoi(ptr);
      ptr = strtok(NULL,"/");
      A.fecha.mes = atoi(ptr);
      ptr = strtok(NULL,"/");
      A.fecha.anio = atoi(ptr);
      ///mostrando el contenido en pantalla
      printf("%2d - ",A.numero);
      printf("%d - ",A.codigo);
      printf("%s - ",A.nombre);
      printf("%.2f - ",A.deuda);
      ///formato de fecha
      switch(A.fecha.mes){
         case 1:
            strcpy(mes,"enero");
            break;
         case 2:
            strcpy(mes,"febrero");
            break;
         case 3:
            strcpy(mes,"marzo");
            break;
         case 4:
            strcpy(mes,"abril");
            break;
         case 5:
            strcpy(mes,"mayo");
            break;
         case 6:
            strcpy(mes,"junio");
            break;
         case 7:
            strcpy(mes,"julio");
            break;
         case 8:
            strcpy(mes,"agosto");
            break;
         case 9:
            strcpy(mes,"septiembre");
            break;
         case 10:
            strcpy(mes,"octubre");
            break;
         case 11:
            strcpy(mes,"noviembre");
            break;
         case 12:
            strcpy(mes,"diciembre");
            break;
      }
      printf("%2d de %s del %4d\n",A.fecha.dia,mes,A.fecha.anio);
      fgets(V,100,F);
   }
   fclose(F);

   printf("\r-------------------------------------------------------------------\n");
   printf("\n");

   system("pause");
}
