//La Oficina de Registro Central y Estadistica de la Universidad Nacional de Ingenieria (ORCE),
//planea la implementacion de un programa que permita a los docentes ingresar los datos de sus
//alumnos y con esta informacion poder brindad resultados estaditicos al final del ciclo.
//
//Disene un programa que le permita ingresar al docente al final de ciclo, mediante un menu,
//los nombres, edad, codigo y nota final de cada estudiante para que luego ORCE pueda leerlos.
//
//Pd: El estudiante aprueba con una nota mayor o igual a 10.

/* Anadimos las bibliotecas */
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdarg.h>

#define MAX_LEN 100                   /*maximo valor que puede tener una linea en el archivo*/

/*Estructura del registro de los estudiantes*/
struct Registro {
  char codigo[10];
  char apellidos[21];
  char nombres[21];
  int edad;
  int notaFinal;
};

/*Prototipo de las funciones a usar*/
int menu();
int leecad(char *cad, int n);
void cargarRegistros(FILE *F, Registro *A, int *dim);
void agregarRegistro(FILE *F, Registro *A, int *dim);
void mostrarRegistros(Registro *A, int dim, int notaMin, int notaMax);
void ordenarApellido(FILE *F, int dim);
void ordenarCodigo(FILE *F, int dim);

void hborder(int cant, ...) {
  va_list lp;
  va_start (lp, cant);
  int len;
  printf("%c", '+');
  for (int n = 0; n < cant; n++) {
    len = va_arg(lp, int);
    printf("%c", '-');
    for (int i = 0; i < len; i++)
      printf("%c", '-');
    printf("%c%c", '-', '+');
  };
  va_end(lp);
  printf ("\n");
}

char *hzborder(int cant, ...) {
  char *cad;
  cad = (char *) malloc((MAX_LEN) * sizeof(char));
  va_list lp;
  va_start (lp, cant);
  int len;
  int pos = 0;
  cad[pos++] = '+';
  for (int n = 0; n < cant; n++) {
    len = va_arg(lp, int);
    cad[pos++] = '-';
    for (int i = 0; i < len; i++)
      cad[pos++] = '-';
    cad[pos++] = '-';
    cad[pos++] = '+';
  };
  va_end(lp);
  cad[pos++] = '\0';
  return cad;
}

/*Funcion principal*/
int main() {
  int n;                             /*el "n" es la opcion seleccionada del menu (se recibe)*/
  char arch[50];
  Registro lista[50];                /*Este arreglo "lista" guardara los datos completos de los estudiantes*/
  int dimLista = -1;                 /* "dimLista" guarda la cantidad de estiantes en la lista*/
  FILE *F;                           /*El archivo "datosAlumnos"*/
  printf("Ingrese el nombre del archivo: ");
  leecad(arch, 50);
  if ((F = fopen(arch,"a+")) == NULL) {
    printf("\nERROR - No se pudo abrir el archivo...\n");
    return 1;
  }
  cargarRegistros(F, lista, &dimLista);

/*Funcion que mostrara que elegi y lo ejecutara*/
  while(true){
    n = menu();               /*Lectura de la opcion del menu*/
    switch (n) {
      case 1:
        printf("\n---------------------------\n\nUsted eligio agregar estudiantes...\n\n");
        agregarRegistro(F, lista, &dimLista);
        break;
      case 2:
        printf("\n---------------------------\n\nUsted eligio mostrar la lista de aprobados...\n\n");
        mostrarRegistros(lista, dimLista, 10, 20);
        break;
      case 3:
        printf("\n---------------------------\n\nUsted eligio mostrar la lista de desaprobados...\n\n");
        mostrarRegistros(lista, dimLista, 0, 9);
        break;
      case 4:
        printf("\n---------------------------\n\nUsted eligio mostrar la lista de estudiantes ordenados por apellido...\n\n");
        ordenarApellido(F, dimLista);
        break;
      case 5:
        printf("\n---------------------------\n\nUsted eligio mostrar la lista de estudiantes ordenados por codigo...\n\n");
        ordenarCodigo(F, dimLista);
        break;
      case 6:
        printf("\n---------------------------\n\nUsted eligio mostrar la lista completa de estudiantes ingresados...\n\n");
        mostrarRegistros(lista, dimLista, 0, 20);
        break;
      case 7:
        fclose(F);
        return 0;
    }
  }
  fclose(F);
  return 0;
}

/*Pantalla del menu, esta funcion devuelve un numero*/
int menu() {
  int opcion;
  do {
    printf("---------------------------\n");
    printf("MENU - Por favor, elija una opcion:\n");
    printf("\n");
    printf("1. Agregar estudiantes\n");
    printf("2. Estudiantes aprobados\n");
    printf("3. Estudiantes desaprobados\n");
    printf("4. Estudiantes ordenados alfabeticamente por Apellido\n");
    printf("5. Estudiantes ordenados por codigo\n");
    printf("6. Mostrar todo\n");
    printf("7. Salir del programa\n");
    printf("\nIngrese una opcion: ");
    scanf("%d", &opcion);
  } while(opcion<1 || opcion>7);
  return opcion;
}

/*Funcion para ingresar cadenas*/
int leecad(char *cad, int n) {
  int i, c;
  c = getchar();
  if (c == EOF) {
    cad[0] = '\0';
    return 0;
  }
  if (c == '\n')
    i = 0;
  else {
    cad[0] = c;
    i = 1;
  }
  for (; i < n-1 && (c = getchar()) != EOF && c != '\n'; i++)
    cad[i] = c;
  cad[i] = '\0';
  if (c != '\n' && c != EOF)
    while ((c = getchar()) != '\n' && c != EOF);
  return 1;
}

/*Recupera los datos del archivo al arreglo*/
void cargarRegistros(FILE *F, Registro *A, int *dim) {
  char linea[MAX_LEN];
  char *ptr;
  *dim = 0;

/*Obtener una linea el archivo*/
  fgets(linea, MAX_LEN, F);
  for (int i = 0; !feof(F); i++) {

/*Se divide la cadena separada por comas con strtok */
    ptr = strtok(linea, ",");
    strcpy(A[i].codigo, ptr);
    ptr = strtok(NULL, ",");
    strcpy(A[i].apellidos, ptr);
    ptr = strtok(NULL, ",");
    strcpy(A[i].nombres, ptr);
    ptr = strtok(NULL, ",");
    A[i].edad = atoi(ptr);
    ptr = strtok(NULL, ",");
    A[i].notaFinal = atoi(ptr);
    *dim = *dim + 1;
    fgets(linea, MAX_LEN, F);
  }
}

/*Agregar mas registros de las notas al archivo y arreglo*/
void agregarRegistro(FILE *F, Registro *A, int *dim) {
  Registro nuevo;
  printf("DATOS DE NUEVO ESTUDIANTE:\n");
  printf("\n");
  printf("Codigo: ");
  leecad(nuevo.codigo, 10);
  printf("Primer apellido: ");
  leecad(nuevo.apellidos, 21);
  printf("Nombre: ");
  leecad(nuevo.nombres, 21);
  printf("Edad: ");
  scanf("%d", &nuevo.edad);
  printf("Nota final: ");
  scanf("%d", &nuevo.notaFinal);
  A[*dim] = nuevo;
  *dim = *dim + 1;
  fprintf(F, "%s,%s,%s,%d,%d\n", nuevo.codigo, nuevo.apellidos, nuevo.nombres, nuevo.edad, nuevo.notaFinal);
}

/*Mostrar los registros que tengan la nota entre notaMin y notaMax, si se pone entre 0 a 20 se muestran todos los registros*/
void mostrarRegistros(Registro *A, int dim, int notaMin, int notaMax) {
  char *linea = hzborder(5, 9, 15, 15, 4, 4);
  char c[2];
  printf(" %s\n", linea);
  printf(" | %-9s | %-15s | %-15s | %4s | %4s |\n", "Codigo", "Apellido", "Nombres", "Edad", "Nota");
  printf(" %s\n", linea);
  for (int i = 0; i < dim; i++) {
    if (A[i].notaFinal >= notaMin && A[i].notaFinal <= notaMax) {
      printf(" | %9s | %-15.15s | %-15.15s | %4d | %4d |\n", A[i].codigo, A[i].apellidos, A[i].nombres, A[i].edad, A[i].notaFinal);
    }
  }
  printf(" %s\n", linea);
  leecad(c,2);
}

/*Cargar los datos en un arreglo auxiliar y ordenarlos por apellido*/
void ordenarApellido(FILE *F, int dim) {
  if (dim < 1) {
    printf("ERROR - Usted no ha agregado ningun estudiante aun...\n");
    return;
  }
  rewind(F);
  Registro copia[50];
  Registro aux;
  cargarRegistros(F, copia, &dim);
  for (int i = 0; i < (dim-1); i++) {
    for (int j = (i+1); j < dim; j++) {
      if(strcmp(copia[i].apellidos,copia[j].apellidos) > 0) {
        aux = copia[i];                    /* "aux", registro para  guardar momentaneamente*/
        copia[i] = copia[j];
        copia[j] = aux;
      }
    }
  }
  mostrarRegistros(copia, dim, 0, 20);
}

/*Cargar los datos en un arreglo auxiliar y ordenarlos por codigo*/
void ordenarCodigo(FILE *F, int dim) {
  if (dim < 1) {
    printf("ERROR - Usted no ha agregado ningun estudiante aun...\n");
    return;
  }
  rewind(F);
  Registro copia[50];
  Registro aux;
  cargarRegistros(F, copia, &dim);
  for (int i = 0; i < (dim-1); i++) {
    for (int j = (i+1); j < dim; j++) {
      if(strcmp(copia[i].codigo,copia[j].codigo) > 0) {
        aux = copia[i];
        copia[i] = copia[j];
        copia[j] = aux;
      }
    }
  }
  mostrarRegistros(copia, dim, 0, 20);
}
