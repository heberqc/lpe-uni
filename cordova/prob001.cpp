#include <conio.h>
#include <iostream.h>

void main(){
    int n, secuencia, mayor=0, veces=0, posiciones=0, ordenado=0;
    cout<<"\tPOBLACION NINOS [1...9]"<<endl;
    cout<<"------------------------------------------------------------"<<endl;
    cout<<"\n\tIngreso total de edades = ";
    cin>>n;
    for(int i=0, d; i<n; i++){
        cout<<"\t\tEdad["<<(i+1)<<"] = ";
        cin>>d;
        secuencia=secuencia*10+d;
        if(d>mayor){
            mayor=d;
            veces=1;
            posiciones=i+1;
        }
        else{
            if(d==mayor){
                veces=veces+1;
                posiciones=posiciones*10+i+1;
            }
        }
        // secuencia ordenada
        if(i==0){
            ordenado=d;
        }
        else{
            if(i==1){
                if(d>ordenado){
                    ordenado=ordenado*10+d;
                }
                else{
                    ordenado=d*10+ordenado;
                }
            }
            else{
                int n1=ordenado, n2=0, multiplo=1;
                for(int r=n1%10; n1>0;){
                    n1=n1/10;
                    if(r>d){
                        n2=r*multiplo+n2;
                    }
                    else{
                        ordenado=((n1*10+r)*10+d)*multiplo+n2;
                        break;
                    }
                    multiplo=multiplo*10;
                    r=n1%10;
                    if(n1==0){
                        ordenado=d*multiplo+n2;
                        break;
                    }
                }
            }
        }
    }
    cout<<"\n\tREPORTES DE EDADES DE NINOS"<<endl;
    cout<<"------------------------------------------------------------"<<endl;
    cout<<"\tMayor      : "<<mayor<<endl;
    cout<<"\tVeces      : "<<veces<<endl;
    cout<<"\tSecuencia  : "<<secuencia<<endl;
    cout<<"\tPosiciones : "<<posiciones<<endl;
    cout<<"\tOrdenado   : "<<ordenado<<endl;
    getch();
}