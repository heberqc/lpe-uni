#include <conio.h>
#include <iostream.h>

void main(){
    int N, d, sec1=0, sec2=0, lon1=0, lon2=0, sum1=0, sum2=0;
    cout<<"\tIngrese n = ";
    cin>>N;
    cout<<"\tIngrese digito a buscar = ";
    cin>>d;
    for(int n=N, r=n%10; n>0;){
        if(r==d){
            sec1=sec1*10+r;
            lon1=lon1+1;
            sum1=sum1+r;
        }
        else{
            if(lon2==0){
                sec2=r;
            }
            else{
                int multiplo=1;
                for(int i=0; i<lon2; i++){
                    multiplo=multiplo*10;
                }
                sec2=r*multiplo+sec2;
            }
            lon2=lon2+1;
            sum2=sum2+r;
        }
        n=n/10;
        r=n%10;
    }
    cout<<"\nSecuencia formada = "<<sec1;
    cout<<"\nTotal de digitos  = "<<lon1;
    cout<<"\nSuma dig. buscado = "<<sum1<<endl;
    cout<<"\nSecuencia No formada = "<<sec2;
    cout<<"\nTotal de digitos     = "<<lon2;
    cout<<"\nSuma dig. No buscado = "<<sum2<<endl;
    getch();
}